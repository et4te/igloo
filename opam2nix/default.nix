{ pkgs ? import <nixpkgs> {}}:
with pkgs;
let
  src = fetchFromGitHub {
    "owner" = "timbertson";
    "repo" = "opam2nix-packages";
    "sha256" = "1bwiz9hjpw3wncvzsqcr8nzwsj37hgm3d04820g1sycs2magqqy6";
    "rev" = "329d165bd5a4bc9fa1b81921b7ec2e8503ec6012";
  };
  opam2nixSrc = fetchFromGitHub {
    "owner" = "timbertson";
    "repo" = "opam2nix";
    "sha256" = "09ski0hbvsxavfdny9zjqd0g37zcxybzflw65p22xlajl1ij75fg";
    "rev" = "b78e185f0f60399ab8a34ce7aae81641ea363ccd";
  };
  opam2nixBin = callPackage "${opam2nixSrc}/nix" {};
in
  callPackage "${src}/nix" { inherit opam2nixBin; }
