let
  # The nixpkgs is pinned to a late staging-18.09 which fixes an issue
  # with rustBuildPackage.
  pkgs = let
    hostPkgs = import <nixpkgs> {};
    pinnedPkgs = hostPkgs.fetchFromGitHub {
      owner = "NixOS";
      repo = "nixpkgs";
      rev = "b97e1dc662bcfb7e48bc19f92b6330144c3fbf06";
      sha256 = "0ax49xal1lrc65s2jjak2355rl79gl64v1kkrmkklh7n34aakizl";
    };
  in import pinnedPkgs {};
in

pkgs.callPackage ./nix {
  opam2nix = pkgs.callPackage ./opam2nix/default.nix {};
}
