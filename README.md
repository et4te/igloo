# igloo

An implementation of Avalanche written in OCaml using MirageOS. The code is
currently academic grade. It is not advisable to use the code in production.

## Premise

The purpose of Igloo is to experiment with cryptocurrency network related
technology and protocol upgrades for the Tezos network such as changes to
transactions, proof of stake and underlying technological infrastructure. 

## Status

The main Avalanche protocol has been implemented, it is missing some optimisations,
tests, re-factorings and will be improved as time goes on.

The optimisations that were attempted:

* Vertices are pruned from the DAG (experimental) in order to keep it constant
  space.
* Chit updates update the ancestry once and cache the score along the vertices of
  the DAG.

## Dependencies

The nix package manager is required in order to build the source code reproducibly.

Install nix and create the following `/etc/nix/nix.conf`.

```conf
sandbox = false

substituters = https://cache.nixos.org https://nixcache.reflex-frp.org

trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI=
```

This will ensure that dependencies are downloaded from cached builds and substantially
speed up compilation times.

## Building

To build igloo, use the following command.

```bash
$ nix-build
```

This will generate binaries in `./result/bin`.

## Running the experiment

The experiment is comprised of at minimum 4 nodes. The bootstrap node propagates a
burst of transactions in the network over UDP. The other nodes, which may bootstrap
off one another will attempt to reach consensus over as many transactions as possible.

```
# Run the bootstrap node
$ result/bin/igloo --socket=127.0.0.1 --port=3001 --generate=true

# Run three extra nodes which connect to the bootstrap node or one another
$ result/bin/igloo --port=3002 --bootstrap_ip=127.0.0.1 --bootstrap_port=3001
$ result/bin/igloo --port=3003 --bootstrap_ip=127.0.0.1 --bootstrap_port=3002
$ result/bin/igloo --port=3004 --bootstrap_ip=127.0.0.1 --bootstrap_port=3003
```

Once the nodes are running the amount of transactions processed per second should
be displayed.
