open Core_kernel

module type TX =
  sig
    type t = {
      data : int list;
      parents : string list;
    } [@@deriving compare, bin_io, sexp]

    include Comparable.S with type t := t
  end

module Transaction : TX =
  struct
    module T = struct
      type t = {
	data : int list;
	parents : string list
      } [@@deriving compare, bin_io, sexp]
    end
    include T
    include Comparable.Poly(T)
  end

module TxSet = Set.Make(Transaction)
