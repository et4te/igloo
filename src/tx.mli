open Core_kernel

module type TX =
  sig
    type t = {
      data : int list;
      parents : string list;
    } [@@deriving compare, bin_io, sexp]

    include Comparable.S with type t := t
  end

module Transaction : TX

module TxSet : Set.S with type Elt.t = Transaction.t
