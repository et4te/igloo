open Core_kernel

module Make (S : Mirage_stack_lwt.V4) = struct

  (* Extension to the network stack to make ip addresses serialize / deserialize. *)
  module S = Stack_ext.Make(S)

  type t = {
    ip : S.ipv4addr;
    port : int;
  } [@@deriving bin_io, sexp]

  let create ip port =
    { ip; port; }

end
