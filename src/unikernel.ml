open Lwt.Infix

module Main (C : Mirage_console_lwt.S) (T : Mirage_time_lwt.S) (MC : Mirage_types.MCLOCK) (R : Mirage_random.S) (S : Mirage_stack_lwt.V4) = struct

  module P = Protocol.Make(C)(T)(MC)(R)(S)
  
  let start c t mc r s =
    let bootstrap_ip = Key_gen.bootstrap_ip () in
    let bootstrap_port = Key_gen.bootstrap_port () in
    let generate_txs = Key_gen.generate () in
    let port = Key_gen.port () in
    let proto = P.create s c mc {
	generate_txs;
	port = port;
	protocol_period = 5000000000;
	round_trip_time = 1000000000;
	indirect_ping_count = 3;
      } in
    let () = P.listen proto in
    match bootstrap_ip with
    | "" ->
       S.listen s
    | ip ->
       let _ = C.log c (Printf.sprintf "Attempting to join node at %s" ip) in
       let _ = P.join proto { ip = (Ipaddr.V4.of_string_exn ip); port = bootstrap_port } in
       S.listen s

end
