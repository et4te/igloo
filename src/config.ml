open Mirage

let bootstrap_ip =
  let doc = Key.Arg.info ~doc:"The ip address of a node to bootstrap with." ["bootstrap_ip"] in
  Key.(create "bootstrap_ip" Arg.(opt string "" doc))

let bootstrap_port =
  let doc = Key.Arg.info ~doc:"The port of a node to bootstrap with." ["bootstrap_port"] in
  Key.(create "bootstrap_port" Arg.(opt int 3001 doc))

let port =
  let doc = Key.Arg.info ~doc:"The port to bind to for listening to messages." ["port"] in
  Key.(create "port" Arg.(opt int 3001 doc))

let generate =
  let doc = Key.Arg.info ~doc:"Whether the node should generate transactions or not." ["generate"] in
  Key.(create "generate" Arg.(opt bool false doc))

let main =
  foreign
    ~keys:[
      Key.abstract bootstrap_ip;
      Key.abstract bootstrap_port;
      Key.abstract port;
      Key.abstract generate;
    ]
    ~packages:[
      package "duration";
      package "core_kernel";
      package "lwt_ppx";
      package "ppx_deriving";
      package "ppx_bin_prot";
      package "ppx_sexp_conv";
      package "ppx_compare";
      package "ppx_hash";
      package "ocamlgraph";
      package "blake2b_simd";
      package "hacl";
    ]
    "Unikernel.Main" (console @-> time @-> mclock @-> random @-> stackv4 @-> job)

let stack = generic_stackv4 default_network

let () =
  register "igloo" [
    main $ default_console $ default_time $ default_monotonic_clock $ default_random $ stack;
  ]
