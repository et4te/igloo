open Core_kernel
open Graph

(* Abstract DAG definition *)

module G = struct

  include Imperative.Digraph.Abstract
	    (
	      struct
		type t = string

		let compare = Pervasives.compare
		let hash = Hashtbl.hash
		let equal = (=)
		let default = ""
	      end
	    )
end
	     
module O = Oper.I(G)
