open Lwt
open Core_kernel
open Conflict_set
open Tx
open Dag

module Make (C : Mirage_console_lwt.S) (MC : Mirage_types.MCLOCK) = struct

  (* Module containing logging functionality *)
  module L = Logging.Make(C)

  (* Security Parameters *)

  let beta1 = ref 11
  let beta2 = ref 11

  (* Test Parameters  *)

  let zero_bytes n =
    let rec accumulate i acc =
      if i >= n then
	acc
      else
	accumulate (i+1) (0::acc)
    in
    accumulate 0 []

  (* Generate 565 bytes as the tx weight *)

  let zl = ref (zero_bytes 565)

  (* Avalanche State *)

  type t = {
    console : C.t;
    conflicts : CT.t;
    vxtbl : (string, string) Hashtbl.Poly.t;
    txtbl : (string, G.V.t) Hashtbl.Poly.t;
    graph : G.t;
    txqueue : Transaction.t Queue.t;
  }

  (* Initialisation *)

  let create console = {
    console;
    conflicts = CT.create;
    vxtbl = Hashtbl.Poly.create ();
    txtbl = Hashtbl.Poly.create ();
    graph = G.create ();
    txqueue = Queue.create ~capacity:1000 ();
  }

  (* Vertices *)

  let hash_tx tx =
    let s = Binable.to_string (module Transaction) tx in
    Blake2b_simd.hash_string s "" 32

  let find_tx_hash t vx =
    Hashtbl.find_exn t.vxtbl (G.V.label vx)

  let find_vx t h =
    Hashtbl.find_exn t.txtbl h

  let add_tx t tx =
    let h = hash_tx tx in
    let v = G.V.create h in
    let l = G.V.label v in
    CT.add t.conflicts h;
    G.add_vertex t.graph v;
    Hashtbl.add_exn t.vxtbl ~key:l ~data:h;
    Hashtbl.add_exn t.txtbl ~key:h ~data:v;
    v

  let remove_vx t v =
    let h = find_tx_hash t v in
    let l = G.V.label v in
    CT.remove t.conflicts h;
    G.remove_vertex t.graph v;
    Hashtbl.remove t.vxtbl l;
    Hashtbl.remove t.txtbl h;
    ()

  (* Branch Preference *)

  let is_preferred t vx =
    let h = find_tx_hash t vx in
    CT.is_preferred t.conflicts h

  let rec is_strongly_preferred t vx =
    let is_parent_preferred vxs =
      match vxs with
      | [] ->
	 true
      | vxs ->
	 List.exists vxs ~f:(fun vx -> is_strongly_preferred t vx)
    in
    if is_preferred t vx then
      is_parent_preferred (G.succ t.graph vx)
    else
      false

  let to_strongly_preferred_list t =
    let f = fun vx acc ->
      if is_strongly_preferred t vx then
	vx::acc
      else
	acc
    in
    G.fold_vertex f t.graph []

  let print_strongly_preferred t =
    let l = to_strongly_preferred_list t in
    for i = 0 to (List.length l - 1) do
      let s = G.V.label (List.nth_exn l i) in
      let _ = L.print t.console (Printf.sprintf "%s" s) in
      ()
    done

  (* Confidence *)

  let get_confidence vx =
    G.Mark.get vx 

  let set_confidence vx c =
    G.Mark.set vx c

  (* Adaptive Parent Selection *)

  let is_fit' conflict vx =
    (Conflict.is_empty conflict) || (get_confidence vx) > 0

  let is_fit t vx =
    let h = find_tx_hash t vx in
    let conflict = CT.find_exn t.conflicts h in
    is_fit' conflict vx

  let select_parents t i =
    let open Transaction in
    let splist = to_strongly_preferred_list t in
    let splist' = List.filter splist ~f:(fun sp -> is_fit t sp) in
    let parents = List.map splist' ~f:(fun sp -> G.succ t.graph sp) in
    List.take
      (List.concat [splist'; List.concat parents]
       |> List.map ~f:(fun vx -> find_tx_hash t vx)
       |> List.permute)
      i

  (* Acceptance *)

  let is_safe_commitment vx =
    (get_confidence vx) >= !beta1

  let rec parents_accepted t vx =
    let is_parent_accepted vx =
      (is_safe_commitment vx) && (parents_accepted t vx)
    in
    match (G.succ t.graph vx) with
    | [] ->
       true
    | vxs ->
       List.for_all vxs ~f:is_parent_accepted

  let is_accepted t vx =
    let h = find_tx_hash t vx in
    let consecutive_counter = CT.get_count t.conflicts h in
    ((is_safe_commitment vx) && (parents_accepted t vx))
    || (consecutive_counter > !beta2)

  (* Ancestor Preference *)

  let update_ancestor t v =
    let h = find_tx_hash t v in
    let p = CT.get_pref t.conflicts h in
    let pv = find_vx t p in
    CT.update_ancestor t.conflicts h (get_confidence v) (get_confidence pv)

  let rec update_ancestors t vx =
    let update_ancestors_loop vxs =
      match vxs with
      | [] -> ()
      | vxs -> List.iter vxs ~f:(fun vx -> update_ancestors t vx)
    in
    if is_accepted t vx then
      begin
	remove_vx t vx
      end
    else
      begin
	update_ancestor t vx;
	update_ancestors_loop (G.succ t.graph vx)
      end

  (* Chit Accumulation *)

  let set_chit t vx =
    let rec update_undecided vx =
      if not (is_accepted t vx) then
	begin
	  let c = get_confidence vx in
	  set_confidence vx (c + 1);
	  List.iter (G.succ t.graph vx) ~f:update_undecided
	end
    in
    set_confidence vx 1;
    List.iter (G.succ t.graph vx) ~f:update_undecided

  (* Receiving Transactions *)

  let is_known t h =
    Hashtbl.mem t.txtbl h

  let add_parent_edge t vx parent_h =
    (* Add the transaction even if no suitable parents 
       exist are found *)
    match (Hashtbl.find t.txtbl parent_h) with
    | Some parent_vx ->
       G.add_edge t.graph vx parent_vx
    | None ->
       ()

  let on_receive_tx t tx =
    let h = hash_tx tx in
    if not (is_known t h) then
      begin
	let vx = add_tx t tx in
	set_confidence vx 0;
	List.iter tx.parents ~f:(fun ph -> add_parent_edge t vx ph);
	Queue.enqueue t.txqueue tx;
      end;
    h
	
  (* Transaction Generation *)

  let generate_tx t =
    let open Transaction in
    let r = Random.int 1000000000 in
    let parents = select_parents t 3 in
    { data = r::(!zl); parents }

  let on_generate_tx t =
    let tx = generate_tx t in
    let _ = on_receive_tx t tx in
    ()

  (* Transaction Queue *)
		  
  let peek_fresh_tx t =
    Queue.peek t.txqueue

  let dequeue_queried_tx t tx =
    let _ = Queue.dequeue_exn t.txqueue in
    ()

  (* Genesis *)

  let create_genesis () =
    let open Transaction in
    { data = !zl; parents = [] }

  let generate_genesis t =
    let genesis = create_genesis () in
    on_receive_tx t genesis

end
