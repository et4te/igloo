open Core_kernel
open Tx

(* A conflict contains a set of transaction ids which have been
   found to be conflicting. If the arity is 1 then there is no
   conflict. 
 *)

module Conflict = struct

  type t = {
    txset : String.Set.t;
    pref : string;
    last : string;
    count : int;
  } 

  let create h = {
    txset = String.Set.singleton h;
    pref = h;
    last = h;
    count = 0;
  }

  let add t h =
    let next_set = String.Set.add t.txset h in
    { t with txset = next_set }

  let is_empty t =
    String.Set.is_empty t.txset

  let is_preferred t h =
    t.pref = h

  let get_pref t =
    t.pref

  let update_pref t h =
    { t with pref = h }

  let get_last t =
    t.last

  let update_last t h =
    { t with last = h; count = 0 }

  let get_count t =
    t.count

  let increment_count t =
    { t with count = t.count + 1 }

end

(* The abstract model of conflicts is represented by a hash table
   where every transaction stores a conflict set. This can be optimised
   when specialising on a particular implementation of transactions but
   in order to keep it abstract it is modelled this way.
 *)
  
module ConflictTable = struct

  type t = (string, Conflict.t) Hashtbl.Poly.t

  let create : t =
    Hashtbl.Poly.create ()

  let add t h =
    match Hashtbl.find t h with
    | Some conflict ->
       let next_conflict = Conflict.add conflict h in
       Hashtbl.set t h next_conflict
    | None ->
       let new_conflict = Conflict.create h in
       Hashtbl.set t h new_conflict

  let remove t h =
    Hashtbl.remove t h

  let find_exn t h =
    Hashtbl.find_exn t h

  exception Not_found_preferred

  let is_preferred t h =
    match Hashtbl.find t h with
    | Some conflict ->
       Conflict.is_preferred conflict h
    | None ->
       raise Not_found_preferred

  exception Not_found_update

  let update_conflict t h ~f =
    match Hashtbl.find t h with
    | Some c ->
       Hashtbl.set t h (f c)
    | None ->
       raise Not_found_update

  let get_pref t h =
    let c = Hashtbl.find_exn t h in
    Conflict.get_pref c

  let update_pref t h =
    let f = fun c -> Conflict.update_pref c h in
    update_conflict t h ~f

  let update_last t h =
    let f = fun c -> Conflict.update_last c h in    
    update_conflict t h ~f

  let increment_count t h =
    update_conflict t h ~f:Conflict.increment_count

  let get_count t h =
    let c = Hashtbl.find_exn t h in
    Conflict.get_count c

  let update_ancestor t h d1 d2 =
    let c = Hashtbl.find_exn t h in
    if d1 > d2 then
      update_pref t h;
    if h <> Conflict.get_last c then
      update_last t h
    else
      increment_count t h

end

(* Abbreviated Names *)

module CT = ConflictTable
