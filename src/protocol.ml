open Lwt
open Core_kernel
open Dag
open Tx       

module Make (C : Mirage_console_lwt.S) (T : Mirage_time_lwt.S) (MC : Mirage_types.MCLOCK) (R : Mirage_random.S) (S : Mirage_stack_lwt.V4) = struct
  module UDP = S.UDPV4

  (* Module containing logging functionality *)
  module L = Logging.Make(C)

  (* Module containing avalanche specific functionality *)
  module A = Avalanche.Make(C)(MC)

  module NetworkAddress = NetworkAddress.Make(S)

  (* Node state *)

  type state = Dead | Alive [@@deriving bin_io, sexp]

  type node = {
    addr : NetworkAddress.t;
    mutable state : state;
  } [@@deriving bin_io, sexp]

  type query_state =
      Success of int
    | Failure
    [@@deriving bin_io, sexp]

  type payload =
    | Ping
    | Ack
    | PingReq of NetworkAddress.t
    | Tx of Transaction.t
    | QueryReq of NetworkAddress.t * Transaction.t
    | QueryRsp of int
    [@@deriving bin_io, sexp]

  type message = {
    seq_no : int;
    payload : payload;
    nodes : node list;
  } [@@deriving bin_io, sexp]

  type config = {
    generate_txs : bool;
    port : int;
    protocol_period : int;
    round_trip_time : int;
    indirect_ping_count : int;
  }

  module BQ
    = Broadcast_queue.Make(
  	  struct
  	    type t = node
  	    type id = NetworkAddress.t
			
  	    let invalidates node node' =
  	      node.addr = node'.addr && node.state <> node'.state

  	    let skip node addr =
  	      node.addr = addr
			    
  	    let size = bin_size_node
  	  end
  	)

  type t = {
    console : C.t;
    clock : MC.t;
    mutable elapsed : int64;
    net : S.t;
    config : config;
    transmit_queue : BQ.t;
    acks : (int, unit Lwt_condition.t) Hashtbl.Poly.t;
    mutable nodes : node list;
    mutable seq_no : int;
    mutable d : int array;
    mutable count : int;
    mutable running_consensus : bool;
    avalanche_acks : (int, int) Hashtbl.Poly.t;
    mutable avalanche : A.t;
  }
	     
  (* Protocol parameters (Initialisation) *)

  let create s c mc ~config = {
    console = c;
    clock = mc;
    elapsed = MC.elapsed_ns mc;
    net = s;
    config;
    nodes = [];
    transmit_queue = BQ.create ();
    acks = Hashtbl.Poly.create ();
    seq_no = 0;
    d = Array.create ~len:2 0;
    count = 0;
    running_consensus = false;
    avalanche_acks = Hashtbl.Poly.create ();
    avalanche = A.create c;
  }

  (* Timing functions *)

  let refresh_timer t =
    t.elapsed <- MC.elapsed_ns t.clock

  let update_sec_timer t =
    let open Int64 in
    let elapsed = MC.elapsed_ns t.clock in
    let d = elapsed - t.elapsed in
    if (of_int (Duration.to_sec d)) > (of_int 1) then
      begin
	t.elapsed <- elapsed;
	true
      end
    else
      false

  (* Utility functions *)

  let next_seq_no t =
    let seq_no = t.seq_no in
    t.seq_no <- t.seq_no + 1;
    seq_no

  let node_count t =
    List.length t.nodes

  let transmit_limit t =
    if node_count t = 0 then
      0
    else
      node_count t
      |> Float.of_int
      |> log
      |> Float.round_up
      |> Float.to_int

  (* Network primitives (send / receive) *)

  let send t ~addr ~payload ~seq_no =
    let transmit_limit = transmit_limit t in
    let nodes = BQ.select_broadcasts t.transmit_queue transmit_limit addr in
    let msg = { seq_no = seq_no; payload; nodes; } in
    (* L.print_message t.console "< Message to %s: %s" addr.ip msg >>= fun () -> *)
    let size = bin_size_message msg in
    (* let _ = L.print t.console (Printf.sprintf "Message size = %d" size) in *)
    let buf = Cstruct.create size in
    ignore(bin_write_message ~pos:0 (Cstruct.to_bigarray buf) msg);
    let udp = S.udpv4 t.net in
    UDP.write ~src_port:t.config.port ~dst:addr.ip ~dst_port:addr.port udp buf

  let rec wait_ack t seq_no =
    let cond = Hashtbl.find_or_add t.acks seq_no ~default:Lwt_condition.create in
    Lwt_condition.wait cond

  let wait_ack_timeout t seq_no timeout =
    pick [
  	(T.sleep_ns (Int64.of_int timeout);%lwt (return `Timeout));
  	(wait_ack t seq_no;%lwt (return `Ok))
      ]

  let rec wait_response t seq_no =
    let cond = Hashtbl.find_or_add t.acks seq_no ~default:Lwt_condition.create in
    Lwt_condition.wait cond(*  >>= fun () ->  *)
    (* (\* let _ = L.print t.console "Finding avalanche_ack" in *\) *)
    (* return (Hashtbl.find_exn t.avalanche_acks seq_no) *)

  let wait_response_timeout t seq_no timeout =
    pick [
	(T.sleep_ns (Int64.of_int timeout) >>= fun () -> return Failure);
	(wait_response t seq_no) >>= fun () -> return (Success 1)
      ]

  (* Node utility functions *)

  let get_node_by_addr t addr =
    List.find ~f:(fun n -> n.addr = addr) t.nodes

  let update_node t node =
    match (get_node_by_addr t node.addr, node.state) with
    | (Some node, Dead) ->
       node.state <- Dead;
       BQ.enqueue_broadcast t.transmit_queue node
    | (None, Alive) ->
       t.nodes <- node::t.nodes;
       BQ.enqueue_broadcast t.transmit_queue node
    | (Some _, Alive)
    | (None, Dead) -> ()

  let random_node t =
    let i = Random.int (List.length t.nodes) in
    List.nth_exn t.nodes i

  let sample t k =
    t.nodes
    |> List.permute
    |> fun l -> List.take l k

  let sample_nodes t k exclude =
    t.nodes
    |> List.filter ~f:(fun n -> n <> exclude)
    |> List.permute
    |> fun l -> List.take l k

  let probe_node t node =
    let seq_no = next_seq_no t in
    ignore_result (send t ~addr:node.addr ~payload:Ping ~seq_no);
    match%lwt (wait_ack_timeout t seq_no t.config.round_trip_time) with
    | `Ok -> return ()
    | `Timeout ->
       L.print t.console "ACK timeout" >>= fun () ->
       let helpers = sample_nodes t t.config.indirect_ping_count node in
       List.iter helpers (fun helper ->
              send t ~addr:helper.addr ~payload:(PingReq node.addr) ~seq_no
	   |> ignore_result
       );
       let wait_time = t.config.protocol_period - t.config.round_trip_time in
       match%lwt wait_ack_timeout t seq_no wait_time with
       | `Ok -> return ()
       | `Timeout ->
	  L.print t.console "Indirect ping timeout" >>= fun () ->
	  let node = { state = Dead; addr = node.addr } in
	  update_node t node;
	  BQ.enqueue_broadcast t.transmit_queue node;
	  return ()

  (* Avalanche utility functions *)

  let query_node t tx node =
    let seq_no = next_seq_no t in
    ignore_result (send t ~addr:node.addr ~payload:(QueryReq (node.addr, tx)) ~seq_no);
    wait_response_timeout t seq_no t.config.round_trip_time

  let rec query_nodes t tx k acc =
    let len = List.length acc in
    if len >= k then
      return acc
    else
      begin
	let samples = sample t (k - len) in
	Lwt_list.map_p (query_node t tx) samples >>= fun queries ->
	let successes =
	  queries
	  |> List.filter ~f:(fun qs -> qs <> Failure)
	  |> List.map
	       ~f:(fun r ->
		   match r with
		   | Success n -> n
		   | Failure -> 0
		  )
	in
	query_nodes t tx k (acc @ successes)
      end

  (* Avalanche main loop *)

  let rec avalanche_loop t k alpha beta =
    match A.peek_fresh_tx t.avalanche with
    | Some tx ->
       let%lwt q = query_nodes t tx k [] in
       let p = List.fold q ~f:(fun xs x -> x + xs) ~init:0 in
       if (float_of_int p) >= (alpha *. float_of_int k) then
	 begin
	   let h = A.hash_tx tx in
	   let vx = A.find_vx t.avalanche h in
	   A.set_chit t.avalanche vx;
	   A.update_ancestors t.avalanche vx;
	   A.dequeue_queried_tx t.avalanche tx;
	   t.count <- t.count + 1;

	   if update_sec_timer t then
	     begin
	       let _ = L.print t.console (Printf.sprintf "TPS = %d" t.count) in
	       t.count <- 0
	     end;

	   avalanche_loop t k alpha beta
	 end
       else
	 begin
	   avalanche_loop t k alpha beta
	 end
    | None ->
       let%lwt () = T.sleep_ns (Int64.of_int (t.config.protocol_period)) in
       refresh_timer t;
       avalanche_loop t k alpha beta

  (* Transaction generation proc *)
  let rec generate_txs t i lim =
    if i >= lim then
      begin
	let%lwt () = L.print t.console "Done." in
	return ()
      end
    else
      begin
	let node = random_node t in
	let tx = A.generate_tx t.avalanche in
	let seq_no = next_seq_no t in
	ignore_result (send t ~addr:node.addr ~payload:(Tx tx) ~seq_no);
	(* let%lwt () = T.sleep_ns 100L in *)
	generate_txs t (i+1) lim
      end
		      
  (* Gossip process *)

  let rec failure_detection t =
    t.nodes <- List.filter t.nodes ~f:(fun node -> node.state = Alive);
    t.nodes <- List.permute t.nodes;
    (if node_count t = 0 then
       T.sleep_ns (Int64.of_int (t.config.protocol_period))
     else
       Lwt_list.iter_s
	 (fun node ->
	  if node.state = Dead then
	    return ()
	  else
	    join [
		probe_node t node >>= fun () ->
		T.sleep_ns (Int64.of_int (t.config.protocol_period))
	      ]
	 ) t.nodes
    ) >>= fun () ->
    (if (node_count t > 2) && (t.running_consensus = false) then
       begin
	 t.running_consensus <- true;
	 (if t.config.generate_txs then
	    begin
	      async (fun () -> generate_txs t 0 30)
	    end
	  else
	    begin
	      let _ = L.print t.console "Running Avalanche loop ..." in
	      async (fun () -> avalanche_loop t 3 0.5 2)
	    end)
       end
    );
    failure_detection t

  let join t addr =
    let seq_no = next_seq_no t in
    send t ~addr ~payload:Ping ~seq_no

  (* Message pump *)

  let handle_payload t src_addr msg =
    let () = update_node t {state=Alive; addr=src_addr} in
    match msg.payload with
    | Ping ->
       send t ~addr:src_addr ~payload:Ack ~seq_no:msg.seq_no
    | PingReq addr ->
       let seq_no = next_seq_no t in
       let%lwt _ = send t ~addr ~payload:Ping ~seq_no in
       (match%lwt wait_ack_timeout t seq_no t.config.protocol_period with
  	| `Timeout ->
	   Lwt_result.return ()
  	| `Ok ->
  	   send t ~addr:src_addr ~payload:Ack ~seq_no:msg.seq_no
       )
    | Ack ->
       (match Hashtbl.find t.acks msg.seq_no with
	| Some cond ->
  	   let () = Lwt_condition.broadcast cond () in
  	   Lwt_result.return ()
	| None ->
  	   (* Unexpected ack -- ignore *)
  	   Lwt_result.return ())
    | Tx tx ->
       if t.config.generate_txs then
	 Lwt_result.return ()
       else
	 begin
	   let h = A.on_receive_tx t.avalanche tx in
	   Lwt_result.return ()
	 end
    | QueryReq (j, tx) ->
       if t.config.generate_txs then
         send t ~addr:src_addr ~payload:(QueryRsp 1) ~seq_no:msg.seq_no
       else
	 begin
	   let h = A.on_receive_tx t.avalanche tx in
	   let v = A.find_vx t.avalanche h in
	   if A.is_strongly_preferred t.avalanche v then
	     begin
               send t ~addr:src_addr ~payload:(QueryRsp 1) ~seq_no:msg.seq_no
	     end
	   else
             send t ~addr:src_addr ~payload:(QueryRsp 0) ~seq_no:msg.seq_no
	 end
    | QueryRsp i ->
       (match Hashtbl.find t.acks msg.seq_no with
	| Some cond ->
	   (* let () = Hashtbl.add_exn t.avalanche_acks msg.seq_no i in *)
	   let () = Lwt_condition.broadcast cond () in
	   Lwt_result.return ()
	| None ->
	   (* Unexpected response -- ignore *)
	   Lwt_result.return ())

  let callback t ~src ~dst ~src_port buf =
    let message = bin_read_message (Cstruct.to_bigarray buf) (ref 0) in
    let () = List.iter message.nodes (update_node t) in
    let addr = NetworkAddress.create src src_port in
    let%lwt _ = handle_payload t addr message in
    return ()

  let listen t =
    Random.self_init ();
    let _ = A.generate_genesis t.avalanche in
    async (fun () -> failure_detection t);
    S.listen_udpv4 t.net ~port:t.config.port (callback t)

end
